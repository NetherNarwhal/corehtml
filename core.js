"use strict";
// Helper methods I have found broadly useful over the years across many different projects. These do extend built-in object prototypes which isn't typically a good idea, however since everyone else avoids them and I don't typically use 3rd party frameworks the chances of conflicts are fairly low.
if (!Array.prototype.remove) Array.prototype.remove = function(item) { const i = this.indexOf(item); if (i > -1) this.splice(i, 1); } // Assumes there is just 1 of item.
if (!Array.prototype.random) Array.prototype.random = function() { return this[Math.floor(Math.random() * this.length)]; }
if (!Array.prototype.shuffle) Array.prototype.shuffle = function() { // Uses https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
    let i, j, x;
    for (i = this.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = this[i]; this[i] = this[j]; this[j] = x;
    }
    return this;
}
function clamp(min, val, max) { if (val < min) return min; if (val > max) return max; return val; }

// Create an HTML or SVG element. (optional) props is {}, (optional) children is Strings and/or Elements. Children may be passed in place of props.
// ex: let node = $e("div", { id: "something", style: "background:red;" }, "Text1", $e("input")); // Text1 and <input> will be children of the newly created element.
// To set dataset attributes, pass them as {"data-my-attr": "myAttr string value"}
// Some SVG elements cannot be created since they conflict with their html versions (ex. "a"). See https://developer.mozilla.org/en-US/docs/Web/SVG/Element.
// For svg, letting the html parser handle the .html string is sometimes better. Ex. with symbols needing to define the viewBox on the symbol instead of the `use` if using $e.
function $e(tag = 'div', props, ...children) { // Children accepts a String for text nodes.
    const el = ($e.svgTags[tag] === true) ? document.createElementNS("http://www.w3.org/2000/svg", tag) : document.createElement(tag);
    if (props) {
        if (Array.isArray(props)) el.append(...props); // Is an array of strings and/or nodes, not properties. Assume they want to append.
        else if (props instanceof window.Node || typeof props !== "object") el.append(props); // Is a DOM node and/or primitives, not properties. Assume they want to append.
        else for (const prop in props) if (props[prop] !== undefined) el.setAttribute(prop, props[prop]); // Common props: class, id, name, style, onclick, src, data-x, viewBox,etc.
    }
    if (children.length > 0 && children[0]) {
        if (children.length > 1) el.append(...children);
        else if (Array.isArray(children[0])) el.append(...children[0]);
        else el.append(children[0]);
    }
    return el;
}
$e.svgTags = {animate:true, animateMotion:true, animateTransform:true, circle:true, clipPath:true, defs:true, desc:true, discard:true, ellipse:true, feBlend:true, feColorMatrix:true, feComponentTransfer:true, feComposite:true, feConvolveMatrix:true, feDiffuseLighting:true, feDisplacementMap:true, feDistantLight:true, feDropShadow:true, feFlood:true, feFuncA:true, feFuncB:true, feFuncG:true, feFuncR:true, feGaussianBlur:true, feImage:true, feMerge:true, feMergeNode:true, feMorphology:true, feOffset:true, fePointLight:true, feSpecularLighting:true, feSpotLight:true, feTile:true, feTurbulence:true, filter:true, foreignObject:true, g:true, hatch:true, hatchpath:true, line:true, linearGradient:true, marker:true, mask:true, metadata:true, mpath:true, path:true, pattern:true, polygon:true, polyline:true, radialGradient:true, rect:true, set:true, stop:true, svg:true, switch:true, symbol:true, text:true, textPath:true, tspan:true, use:true, view:true}; // Not supported due to naming conflicts with html elements: a, image (is an alias for img), script, style, title.

// Adds some syntactical sugar to the built-in EventTarget. These will be inherited by Element, Document, ShadowRoot, etc.
if (!EventTarget.prototype.on) EventTarget.prototype.on = function(type, listener, options) { // Type can be an array. Pass `once: true` in options for one-time listeners.
    if (Array.isArray(type)) for (const t of type) this.addEventListener(t, listener, options); else this.addEventListener(type, listener, options); return this;
}
if (!EventTarget.prototype.off) EventTarget.prototype.off = function(type, listener, options) { // Type can be an array. Pass same listeners & options used with on.
    if (Array.isArray(type)) for (const t of type) this.removeEventListener(t, listener, options); else this.removeEventListener(type, listener, options); return this;
}
// Adds some syntactical sugar to the built-in Element (https://developer.mozilla.org/en-US/docs/Web/API/Element) by making the common methods chainable.
if (!Element.prototype.add) Element.prototype.add = function(...nodesOrDOMStrings) { this.append(...nodesOrDOMStrings); return this; }
if (!Element.prototype.empty) Element.prototype.empty = function() { this.innerHTML = ''; return this; } // TODO: Use `this.replaceChildren()` instead?
if (!Element.prototype.query) Element.prototype.query = function(selector) { return this.querySelector(selector) }
if (!Element.prototype.queryAll) Element.prototype.queryAll = function(selector) { return this.querySelectorAll(selector) }
if (!Element.prototype.addClass) Element.prototype.addClass = function(...className) { this.classList.add(...className); return this; }
if (!Element.prototype.removeClass) Element.prototype.removeClass = function(...className) { this.classList.remove(...className); return this; }
if (!Element.prototype.is) Element.prototype.is = function(className) { return this.classList.contains(className) }
if (!Element.prototype.getProp) Element.prototype.getProp = function(prop) { let val = getComputedStyle(this).getPropertyValue(prop); if (val) val=val.trim(); return val; }
if (!Element.prototype.setProp) Element.prototype.setProp = function(prop, value) { this.style.setProperty(prop, value); return this; }
if (!Element.prototype.html) Object.defineProperty(Element.prototype, 'html', {get: function() { return this.innerHTML }, set: function(html) { this.innerHTML = html }});
if (!Element.prototype.text) Object.defineProperty(Element.prototype, 'text', {get: function() { return this.textContent }, set: function(text) { this.textContent = text }});
// Adds some syntactical sugar to the built-in Document. Document doesn't extend Element so not all extensions apply.
if (!Document.prototype.add) Document.prototype.add = Element.prototype.add;
if (!Document.prototype.query) Document.prototype.query = Element.prototype.query;
if (!Document.prototype.queryAll) Document.prototype.queryAll = Element.prototype.queryAll;
if (!Document.prototype.getProp) Document.prototype.getProp = function(prop) {let val = getComputedStyle(this.documentElement).getPropertyValue(prop); if (val) val=val.trim(); return val; }
if (!Document.prototype.setProp) Document.prototype.setProp = function(prop, value) { this.documentElement.style.setProperty(prop, value); return this; }
// Adds some syntactical sugar to the built-in DocumentFragment, and therefore ShadowRoot. DocumentFragment doesn't extend Element or Document so not all extensions apply.
if (!DocumentFragment.prototype.add) DocumentFragment.prototype.add = Element.prototype.add;
if (!DocumentFragment.prototype.query) DocumentFragment.prototype.query = Element.prototype.query;
if (!DocumentFragment.prototype.queryAll) DocumentFragment.prototype.queryAll = Element.prototype.queryAll;